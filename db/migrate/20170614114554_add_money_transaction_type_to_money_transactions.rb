class AddMoneyTransactionTypeToMoneyTransactions < ActiveRecord::Migration[5.0]
  def change
    add_reference :money_transactions, :money_transaction_type, foreign_key: true
  end
end
