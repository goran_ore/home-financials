class CreateCategories < ActiveRecord::Migration[5.0]
  def change
    create_table :categories, :id => false do |t|
      t.integer :id, :options => 'PRIMARY KEY'
      t.string :name
      t.string :description
    end
  end
end
