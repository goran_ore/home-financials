class CreateMoneyTransactionTypes < ActiveRecord::Migration[5.0]
  def change
    create_table :money_transaction_types, :id => false do |t|
      t.integer :id, :options => 'PRIMARY KEY'
      t.string :name
    end
  end
end
