class AddAccountToMoneyTransactions < ActiveRecord::Migration[5.0]
  def change
    add_reference :money_transactions, :account, foreign_key: true
  end
end
