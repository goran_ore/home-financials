class AddCategoryToMoneyTransactions < ActiveRecord::Migration[5.0]
  def change
    add_reference :money_transactions, :category, foreign_key: true
  end
end
