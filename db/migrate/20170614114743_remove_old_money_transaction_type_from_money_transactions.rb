class RemoveOldMoneyTransactionTypeFromMoneyTransactions < ActiveRecord::Migration[5.0]
  def change
    remove_column :money_transactions, :money_transaction_type, :string
  end
end
