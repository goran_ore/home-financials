class RemoveOldAccountFromMoneyTransactions < ActiveRecord::Migration[5.0]
  def change
    remove_column :money_transactions, :account, :string
  end
end
