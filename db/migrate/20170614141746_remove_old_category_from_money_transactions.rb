class RemoveOldCategoryFromMoneyTransactions < ActiveRecord::Migration[5.0]
  def change
    remove_column :money_transactions, :category, :string
  end
end
