class AddMoneyTransactionTypeToMoneyTransaction < ActiveRecord::Migration[5.0]
  def change
    add_column :money_transactions, :money_transaction_type, :string
  end
end
