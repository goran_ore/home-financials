class CreateAccounts < ActiveRecord::Migration[5.0]
  def change
    create_table :accounts, :id => false do |t|
      t.integer :id, :options => 'PRIMARY KEY'
      t.string :name
      t.string :description
    end
  end
end
