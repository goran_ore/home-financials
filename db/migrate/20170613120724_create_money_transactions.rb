class CreateMoneyTransactions < ActiveRecord::Migration[5.0]
  def change
    create_table :money_transactions do |t|
      t.date :date
      t.decimal :value
      t.string :description
      t.string :category
      t.string :account

      t.timestamps
    end
  end
end
