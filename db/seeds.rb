# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
MoneyTransactionType.destroy_all
MoneyTransactionType.create!(id: 1, name: "Expense")
MoneyTransactionType.create!(id: 2, name: "Income")
MoneyTransactionType.create!(id: 3, name: "Transfer")

Category.destroy_all
Category.create!(id: 1, name: "Sport")
Category.create!(id: 2, name: "Food")
Category.create!(id: 3, name: "Cigarete")
Category.create!(id: 4, name: "Salary")
Category.create!(id: 5, name: "Drink")
Category.create!(id: 6, name: "Pets")
Category.create!(id: 7, name: "Education")

Account.destroy_all
Account.create!(id: 1, name: "Bank account")
Account.create!(id: 2, name: "Amex")
Account.create!(id: 3, name: "Cash")
