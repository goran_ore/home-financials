json.extract! money_transaction, :id, :date, :value, :description, :category, :account, :created_at, :updated_at, :money_transaction_type
json.url money_transaction_url(money_transaction, format: :json)
