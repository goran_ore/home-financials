module MoneyTransactionsHelper
  #hold the logic of converting the tags to links (look: _money_transaction.html.erb)
  def tag_links(tags)
    tags.split(",").map{|tag| link_to tag.strip, tag_path(tag.strip) }.join(", ")
  end

  def transaction_link(money_transaction)
    link_to (money_transaction.category.name + " (" + money_transaction.transaction_prefix + money_transaction.value.to_s) + " kn)", money_transaction_path(money_transaction)
  end
end
