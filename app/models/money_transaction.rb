class MoneyTransaction < ApplicationRecord
  belongs_to :money_transaction_type
  belongs_to :category
  belongs_to :account
  has_many :taggings
  has_many :tags, through: :taggings
  validates :money_transaction_type_id, presence: true
  validates :category_id, presence: true
  validates :account_id, presence: true

  #Search of tagged transactions
  def self.tagged_with(name)
    Rails.logger.debug "MoneyTransaction self.tagged_with(name): #{name}"
    Tag.find_by_name!(name).money_transactions
  end

  def transaction_prefix
    case money_transaction_type_id
    when 1
      "-"
    when 2
      "+"
    else
      "+/-"
    end
  end

  #setter attribute, returns selected tag names or creates them if non existing
  def all_tags=(names)
    self.tags = names.split(",").map do |name|
      Tag.where(name: name.strip).first_or_create! #finds a record, if it doesn't exist, creates it
    end
  end

  #getter
  def all_tags
    self.tags.map(&:name).join(",")
  end
end
