class Category < ApplicationRecord
  self.primary_key = "id"
  has_many :money_transactions
  validates :id, presence: true
  validates :name, presence: true
end
