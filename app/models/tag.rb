class Tag < ApplicationRecord
  has_many :taggings
  has_many :money_transactions, through: :taggings
end
