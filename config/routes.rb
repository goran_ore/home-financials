Rails.application.routes.draw do
  resources :money_transactions
  root  'money_transactions#index'
  get 'tags/:tag', to: 'money_transactions#index', as: "tag"
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
